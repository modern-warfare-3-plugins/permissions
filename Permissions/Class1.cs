﻿using Addon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

namespace Permissions
{
    public class Main : CPlugin
    {
        private List<string> _userGroups;
        private List<ConfigurationOption> _config; 

        /// <summary>
        /// Executes when the MW3 server loads
        /// </summary>
        public override void OnServerLoad()
        {
            ServerPrint("Permission plugin loaded. Author: Pozzuh. Version 1.4");
            ServerPrint("Permission plugin updated by SgtLegend");

            // First thing we need to do is construct the configuration for the permissions plugin
            CheckAndSetPermissionConfig();

            // Next we need to initialize the user groups
            InitUserGroups();
        }

        /// <summary>
        /// Exeutes when a player types something in the chat
        /// </summary>
        /// <param name="message"></param>
        /// <param name="client"></param>
        /// <param name="teamchat"></param>
        /// <returns></returns>
        public override ChatType OnSay(string message, ServerClient client, bool teamchat)
        {
            // Get the command
            string command = message.ToLower().Split(',')[0];

            // Get the group that the user is in and the commands allowed by that group
            string userGroup = GetUserGroup(client.XUID);
            List<string> groupCommands = GetGroupValue(userGroup, "commands");

            // Execute the command
            switch (command.Substring(1))
            {
                case "getxuid":
                case "gettype":
                    TellClient(client.ClientNum,
                               (command == "!getxuid"
                                    ? string.Format("Your XUID is: {0}", client.XUID)
                                    : string.Format("Your user type is: {0}", userGroup)), true);

                    return ChatType.ChatNone;

                case "help":
                case "cmdlist":
                    string allowedCommands = "^1You can use: ^7";

                    // Join the commands by a seperated comma
                    allowedCommands += string.Join("^1, ^7", groupCommands.ToArray());

                    // Check the length of the "allowedCommands", if it's total length doesn't exceed 143 characters
                    allowedCommands = ReplaceLastCommand(allowedCommands) + "...";
                    
                    TellClient(client.ClientNum, allowedCommands, true);
                    return ChatType.ChatNone;
            }

            // Check if the player is protected against the given command
            string[] protectedXuids = GetServerCFG("Permission", "Protected_xuids", "").Split(',');
            string[] protectedCommands = GetServerCFG("Permission", "Protected_commands", "").Split(',');
            ServerClient requestedPlayer = protectedXuids.Length > 0 ? DeterminePlayer(message.ToLower()) : null;

            if (requestedPlayer != null && protectedCommands.Length > 0)
            {
                if (protectedCommands.Any(x => x == command && protectedXuids.Contains(requestedPlayer.XUID)))
                {
                    TellClient(client.ClientNum,
                               string.Format(
                                   "[^1Permissions^7] ^:You aren't allowed to use ^7{0} ^:command against ^7{1}",
                                   command, requestedPlayer.Name), true);

                    return ChatType.ChatNone;
                }
            }

            // Finally check the immune groups against the current user, if the user is trying to use a command
            // they aren't allowed to be using let them know that
            string[] immuneGroups = GetServerCFG("Permission", "Immune_groups", "Admin").Split(',');

            if (!immuneGroups.Contains(userGroup) && command.StartsWith("!") && !groupCommands.Contains(command))
            {
                TellClient(client.ClientNum, "[^1Permissions^7] ^:You aren't allowed to use that command!", true);
                return ChatType.ChatNone;
            }

            return ChatType.ChatContinue;
        }

        /// <summary>
        /// Sets all the default configuration data then attempts to collect the user configured data
        /// from the sv_config.ini file that exists on the server
        /// </summary>
        private void CheckAndSetPermissionConfig()
        {
            _config = new List<ConfigurationOption>
                {
                    new ConfigurationOption
                        {
                            Name = "Usergroups",
                            Value = "Admin,Moderator,User"
                        },
                    new ConfigurationOption
                        {
                            Name = "Admin_xuids",
                            Value = ""
                        },
                    new ConfigurationOption
                        {
                            Name = "Admin_commands",
                            Value = "*ALL*"
                        },
                    new ConfigurationOption
                        {
                            Name = "Moderator_xuids",
                            Value = ""
                        },
                    new ConfigurationOption
                        {
                            Name = "Moderator_commands",
                            Value = "!help,!getxuid,!gettype"
                        },
                    new ConfigurationOption
                        {
                            Name = "User_xuids",
                            Value = "*EVERYONE*"
                        },
                    new ConfigurationOption
                        {
                            Name = "User_commands",
                            Value = "!help,!getxuid,!gettype"
                        }
                };

            foreach (ConfigurationOption c in _config)
            {
                string configValue = GetServerCFG("Permission", c.Name, "xx__xx");

                if (configValue == "xx__xx")
                {
                    SetServerCFG("Permission", c.Name, c.Value);
                }
                else
                {
                    c.Value = configValue;
                }
            }
        }

        /// <summary>
        /// Retrieves the user group configuration for the permission plugin
        /// </summary>
        private void InitUserGroups()
        {
            _userGroups = new List<string>((GetConfigValue("UserGroups") ?? string.Empty).Split(','));
        }

        /// <summary>
        /// Retrieves the group name for the given player XUID
        /// </summary>
        /// <param name="xuid"></param>
        /// <returns></returns>
        private string GetUserGroup(string xuid)
        {
            foreach (string group in _userGroups)
            {
                List<string> usersInGroup = GetGroupValue(group, "xuids");

                if (usersInGroup.Contains(xuid) || (usersInGroup[0] != null && usersInGroup[0] == "*EVERYONE*"))
                {
                    return group;
                }
            }

            return "User";
        }

        /// <summary>
        /// Retrieves the given value type for the given group name
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private List<string> GetGroupValue(string groupName, string type)
        {
            groupName = groupName + "_" + type;

            // Attempt to get the value from the configuration
            string groupPlayers = GetConfigValue(groupName);

            if (groupPlayers == null)
            {
                groupPlayers = GetServerCFG("Permission", groupName, "");

                _config.Add(new ConfigurationOption
                    {
                        Name = groupName,
                        Value = groupPlayers
                    });
            }

            return new List<string>(groupPlayers.Split(','));
        }

        /// <summary>
        /// Attempts to return the configuration string for the given configuration name, if it
        /// can't find the value it will simply return a null value
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private string GetConfigValue(string name)
        {
            ConfigurationOption configOpt = _config.SingleOrDefault(x => x.Name == name);
            return configOpt != null ? configOpt.Value : null;
        }

        /// <summary>
        /// Checks the length of the given string and ensures it always stays under 143 characters so
        /// everything fits nicely in the users chat
        /// </summary>
        /// <param name="commands"></param>
        /// <returns></returns>
        private static string ReplaceLastCommand(string commands)
        {
            if (commands.Length > 143)
            {
                commands = commands.Substring(0, commands.LastIndexOf("^1,^7", StringComparison.InvariantCulture));
            }

            return (commands.Length > 143) ? ReplaceLastCommand(commands) : commands;
        }

        /// <summary>
        /// Attempts to match the given chat message against a list of clients currently logged on to
        /// the server
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private ServerClient DeterminePlayer(string msg)
        {
            return (from m in msg.Split(',')
                    from client in GetClients()
                    where (client.ClientNum.ToString(CultureInfo.InvariantCulture) == m || client.Name.ToLower() == m || client.Name.ToLower().Contains(m))
                    select client).FirstOrDefault();
        }
    }

    /// <summary>
    /// A simple class to handle the configuration options
    /// </summary>
    internal class ConfigurationOption
    {
        public string Name, Value;
    }
}